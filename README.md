# FlaskAuth

Very simple Flask auth app for my buddy @Bubbles on DevRant.

No frills, no style, no need to do anything. Migrations are already run, DB is included. Everything used is based off of the amazing blog by 
the amazing [Miguel Grinberg](https://blog.miguelgrinberg.com/post/the-flask-mega-tutorial-part-i-hello-world) so he's really the
one you should thank, being the master of Flask that he is.

Enjoy, Charon92

```
# To begin, download into directory then run:
source backend/venv/bin/activate

# This should start the environment. Next, install the dependencies:
pip install -r requirements.txt

# NOTE: this should all be run from the project root directory and it's assuming your using a *nix system
# After that, you're good to go. Happy learning!
```